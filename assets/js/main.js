
    // Main Js

    $(document).ready(function () {

        // KVKK Cookie Check
        var kvkk = $.cookie("kvkk");
        if (kvkk == 1) {
            $('.kvkk-box').hide();
        }

        // Home Slider
        var owl = $('#homeSlider');
        owl.owlCarousel({
            navigation : true,
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem: true,
            items : 1,
            nav : false,
            dots: false,
            autoplayTimeout: 4000,
            autoplay: true,
            loop: true,
        });
        $.ajax({
            url: '/json/data.json',
            dataType: 'json',
            success: function(data) {
                var content = '';
                for (i in data.owl) {
                    content = data.owl[i].item;
                    owl.trigger('add.owl.carousel',content);
                }

                owl.trigger('refresh.owl.carousel');
            }
        });

        // Mobile Menu
        $(function () {
            'use strict'

            $("[data-trigger]").on("click", function(){
                var trigger_id =  $(this).attr('data-trigger');
                $(trigger_id).toggleClass("show");
                $('body').toggleClass("offcanvas-active");
            });

            // close if press ESC button
            $(document).on('keydown', function(event) {
                if(event.keyCode === 27) {
                    $(".navbar-collapse").removeClass("show");
                    $("body").removeClass("overlay-active");
                }
            });

            // close button
            $(".btn-close").click(function(e){
                $(".navbar-collapse").removeClass("show");
                $("body").removeClass("offcanvas-active");
            });
        });

        // KVKK Close
        $('#kvkk-close').click(function () {
            $.cookie("kvkk", 1, {
                expires: 365,
                path    : '/'
            });

            $('.kvkk-box').hide();
        });

        // Abuse Form
        $(function () {
            $('#newsletterForm').on('submit', function (e) {
                e.preventDefault();
                $.ajax({
                    type: 'post',
                    url: '/includes/newsletter.php',
                    data: $('#newsletterForm').serialize(),
                    success : [
                        function(response) {
                            if (response == 0) {
                                Swal.fire(
                                    'Oops!',
                                    'E-Bülten kaydınız gerçekleştirilemedi. Lütfen daha sonra tekrar deneyiniz.',
                                    'error'
                                )
                            } else if (response == 1) {
                                Swal.fire(
                                    'Başarılı!',
                                    'E-Bülten aboneliğiniz başarıyla gerçekleştirildi.',
                                    'success'
                                )
                            } else if (response == 2) {
                                Swal.fire(
                                    'Oops!',
                                    'Bu E-Posta adresi kayıtlı. Lütfen başka bir e-posta adresi ile deneyiniz.',
                                    'error'
                                )
                            } else {
                                Swal.fire(
                                    'Oops!',
                                    'E-Bülten kaydınız gerçekleştirilemedi. Lütfen daha sonra tekrar deneyiniz.',
                                    'error'
                                )
                            }
                        }
                    ]
                });
            });
        });

    });