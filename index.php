<?php include "header.php"; ?>

    <section id="pageContent">

        <div id="homeSlider" class="owl-carousel owl-theme">
        </div>

        <div class="space"></div>

        <div class="container">
            <h4 class="text-center font-weight-600 mb-1">550'den fazla eviye çeşidi</h4>
            <p class="text-center font-weight-300 f-size-14 mb-5">Paslanmaz çelik, granit ve cam eviye çeşitleri ile sizlere geniş bir ürün yelpazesi sunuyoruz.</p>

            <div class="discount-banner-box">
                <img class="img-fluid" src="/assets/img/ankastre-indirim.jpg" alt="Ankastre Set İndirimi">
            </div>

        </div>

        <div class="space"></div>

    </section>

<?php include "footer.php"; ?>