<?php include "header.php"; ?>

    <div id="pageContent">
        <div class="w-100 pageTopArea bg-img" style="background-image: url('assets/img/banner.png');">
            <div class="container vertical-center">
                <h1 class="text-white">Aksesuarlar</h1>
            </div>
        </div>
        <div class="container">
            <div class="space"></div>

                <div class="row">
                    <?php
                        $products = $db->query("SELECT * FROM products", PDO::FETCH_ASSOC);
                        if ( $products->rowCount() ){
                            foreach( $products as $product ){
                    ?>
                        <div class="col-md-3 col-6 mb-4">
                            <div class="card h-100">
                                <div class="card-body">
                                    <a href="product.php?id=<?=$product['id'] ?>">
                                        <img class="img-fluid" src="<?=$product['image_1'] ?>" alt="<?=$product['name'] ?>">
                                        <p class="f-size-14 text-dark font-weight-600 mt-3"><?=$product['name'] ?></p>
                                        <span class="f-size-18 text-primary font-weight-600"><?=$product['price_list'].' '.$product['currency'] ?></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php } } ?>
                </div>

            <div class="space"></div>
        </div>
    </div>

<?php include "footer.php"; ?>