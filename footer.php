<footer>
    <div class="container">
        <div class="row justify-content-center newsletter-box">
            <h5 class="d-inline-block mb-0">E-BÜLTENE ABONE OLUN, GELİŞMELERİ KAÇIRMAYIN</h5>
            <form id="newsletterForm" class="form-inline my-2 my-lg-0">
                <input class="form-control" name="email" type="email" placeholder="E-Posta Adresiniz" aria-label="E-Posta Adresiniz" required>
                <button class="btn btn-danger" type="submit">
                    <i class="fal fa-long-arrow-right"></i>
                </button>
            </form>
        </div>
    </div>
    <div class="w-100">
        <hr />
    </div>
    <div class="container pb-5">
        <div class="row">
            <div class="col-md-6 col-12">
                <div class="row">
                    <div class="col-md-6 col-12">
                        <ul class="pl-0 mb-4">
                            <li>KURUMSAL</li>
                            <li><a href="#">Sıkça Sorulan Sorular</a></li>
                            <li><a href="#">Yetkili Servisler</a></li>
                            <li><a href="#">İletişim</a></li>
                            <li><a href="#">Kişisel Verilerin Korunması</a></li>
                            <li><a href="#">Hakkımızda</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Kullanıcı Rehberi</a></li>
                            <li><a href="#">Mesafeli Satış Sözleşmesi</a></li>
                            <li><a href="#">Evdekal Türkiye</a></li>
                            <li><a href="#">Ek Garanti Belgesi</a></li>
                            <li><a href="#">Hakkımızda</a></li>
                        </ul>

                        <ul class="pl-0 mb-4">
                            <li>KAMPANYA</li>
                            <li><a href="#">Ankastre Cihazlar</a></li>
                            <li><a href="#">Kampanyalar</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-12">
                        <ul class="pl-0 mb-4">
                            <li>ÜRÜNLER</li>
                            <li><a href="#">Eviye</a></li>
                            <li><a href="#">Fırın</a></li>
                            <li><a href="#">Ocak</a></li>
                            <li><a href="#">Armatür</a></li>
                            <li><a href="#">Davlumbaz</a></li>
                        </ul>

                        <ul class="pl-0 mb-4">
                            <li>MEDYA</li>
                            <li><a href="#">Tanıtım Filmi</a></li>
                            <li><a href="#">Belgeler ve Sertifikalar</a></li>
                            <li><a href="#">Panaromik Ukinox</a></li>
                            <li><a href="#">Kurumsal Logo</a></li>
                        </ul>

                        <ul class="pl-0 mb-4">
                            <li>Bizi Takip Edin</li>
                            <li class="d-inline-block mr-2 footerSocial"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li class="d-inline-block mr-2 footerSocial"><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li class="d-inline-block mr-2 footerSocial"><a href="#"><i class="fab fa-youtube"></i></a></li>
                            <li class="d-inline-block mr-2 footerSocial"><a href="#"><i class="fab fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-12">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12498.459757130735!2d27.1790466!3d38.4503623!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa7dcc7bf74c7e2cb!2zQWvEsWxsxLEgVGljYXJldCBFLVRpY2FyZXQgw4fDtnrDvG1sZXJp!5e0!3m2!1str!2str!4v1601460714739!5m2!1str!2str" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
        </div>
    </div>
</footer>

<div class="kvkk-box">
    <p class="mb-0">Alışveriş deneyiminizi iyileştirmek için yasal düzenlemelere uygun çerezler (cookies) kullanıyoruz. Detaylı bilgiye
        <a href="#">Çerez Politikası</a>
        sayfamızdan erişebilirsiniz.</p>
    <i id="kvkk-close" class="fal fa-times-square cursor-pointer"></i>
</div>

<script src="/vendor/jquery/jquery.min.js" type="text/javascript"></script>
<script src="/vendor/jquery/jquery.cookie.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.3.5/sweetalert2.all.min.js" integrity="sha512-rQGS49+CfE3nYVbZ4JFwdUrwZwHMnvNz611lVFevMeKN8HG7z/Sep0K91rjMbL4da6VSmOxk4hSXrhK0M+nDnQ==" crossorigin="anonymous"></script>        <script src="/vendor/bootstrap/js/tether.min.js" type="text/javascript"></script>
<script src="/vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/vendor/owl-carousel/js/owl.carousel.min.js" type="text/javascript"></script>
<script src="/assets/js/main.js" type="text/javascript"></script>
</body>
</html>