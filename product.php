<?php
    include "header.php";
    $product_id = $_GET['id'];
    $product_info = $db->query("SELECT * FROM products WHERE id = '{$product_id}'")->fetch(PDO::FETCH_ASSOC);
?>


    <div id="pageContent">
        <div class="w-100 pageTopArea bg-img" style="background-image: url('assets/img/banner.png');">
            <div class="container vertical-center">
                <h1 class="text-white"><?=$product_info['name'] ?></h1>
                <p class="text-white"><?=$product_info['category_path'] ?></p>
            </div>
        </div>
        <div class="container">
            <div class="row mt-4">
                <div class="col-sm-5">
                    <div id="carousel" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="<?=$product_info['image_1'] ?>">
                            </div>
                            <div class="carousel-item">
                                <img src="<?=$product_info['image_2'] ?>">
                            </div>
                            <div class="carousel-item">
                                <img src="<?=$product_info['image_3'] ?>">
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div id="thumbcarousel" class="carousel slide" data-interval="false">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <div data-target="#carousel" data-slide-to="0" class="thumb"><img src="<?=$product_info['image_1'] ?>"></div>
                                    <div data-target="#carousel" data-slide-to="1" class="thumb"><img src="<?=$product_info['image_2'] ?>"></div>
                                    <div data-target="#carousel" data-slide-to="2" class="thumb"><img src="<?=$product_info['image_3'] ?>"></div>
                                </div>
                                <!-- /item -->
                            </div>
                            <!-- /carousel-inner -->
                            <a class="carousel-control-prev" href="#thumbcarousel" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#thumbcarousel" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                        <!-- /thumbcarousel -->
                    </div>
                    <!-- /clearfix -->
                </div>
                <div class="col-sm-7">
                    <h2><?=$product_info['name'] ?></h2>
                    <h3 class="f-size-18">Ürün Kodu: <?=$product_info['code'] ?></h3>
                    <?=$product_info['detail'] ?>
                </div>
            </div>
        </div>
    </div>

<?php include "footer.php"; ?>