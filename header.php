<?php require_once "includes/conn.php"; ?>
<!DOCTYPE html>
<!--[if lte IE 6]><html class="preIE7 preIE8 preIE9"><![endif]-->
<!--[if IE 7]><html class="preIE8 preIE9"><![endif]-->
<!--[if IE 8]><html class="preIE9"><![endif]-->
<!--[if gte IE 9]><!--><html><!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>E-Ticaret Yazılımı V1</title>

    <link rel="stylesheet" href="/vendor/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="/vendor/fontAwesome/all.css">
    <link rel="stylesheet" href="/vendor/owl-carousel/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/vendor/owl-carousel/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.3.5/sweetalert2.min.css" integrity="sha512-NU255TKQ55xzDS6UHQgO9HQ4jVWoAEGG/lh2Vme0E2ymREox7e8qwIfn6BFem8lbahhU9E2IQrHZlFAxtKWH2Q==" crossorigin="anonymous" />
    <link rel="stylesheet" href="/assets/css/main.css">
    <link rel="stylesheet" href="/assets/css/responsive.css">

    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet">
</head>
<body>

<header>
    <div class="container">
        <div id="topbar">
            <div class="col-md-12">
                <ul class="pl-0 mb-0 text-md-right">
                    <li class="list-inline d-inline-block mr-2 mr-md-3">
                        <a href="#"><i class="mr-2 fab fa-facebook-f"></i></a>
                        <a href="#"><i class="mr-2 fab fa-twitter"></i></a>
                        <a href="#"><i class="mr-2 fab fa-youtube"></i></a>
                        <a href="#"><i class="fab fa-instagram"></i></a>
                    </li>
                    <li class="list-inline d-inline-block mr-2 mr-md-3">
                        <a href="tel:908502000880">
                            +90 (850) 200 08 80
                        </a>
                    </li>
                    <li class="list-inline d-inline-block mr-md-3">
                        <a class="text-dark" href="mailto:bilgi@xxxx.com.tr">
                            bilgi@xxxx.com.tr
                        </a>
                    </li>
                    <li class="list-inline mr-2 d-none d-md-inline-block">
                        <a class="font-weight-600 text-dark" href="#">
                            <i class="fal fa-user mr-2"></i> Giriş
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <nav class="navbar navbar-expand-lg navbar-light bg-transparent">
            <a class="navbar-brand" href="#">
                <img src="/assets/img/logo.png" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-trigger="#main_nav">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="main_nav">
                <div class="offcanvas-header">
                    <button class="btn btn-outline-light btn-close float-right"> &times Kapat</button>
                    <h5 class="py-2 text-white">Menü</h5>
                </div>
                <div class="w-100 mobileAuthButtonBox">
                    <div class="row">
                        <div class="w-50">
                            <a class="btn btn-block btn-outline-dark authButtton" href="#">Giriş Yap</a>
                        </div>
                        <div class="w-50">
                            <a class="btn btn-block btn-outline-dark authButtton" href="#">Kayıt Ol</a>
                        </div>
                    </div>
                </div>

                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown mega-menu">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            KOLEKSİYONLAR
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <div class="row m-0">
                                <div class="col-md-4 col-6">
                                    <div class="mega-menu-item row m-0">
                                        <a href="#">
                                            <img src="/assets/img/menu/eviye.svg" alt="Eviye">
                                            <h5>EVİYELER</h5>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-6">
                                    <div class="mega-menu-item row m-0">
                                        <a href="#">
                                            <img src="/assets/img/menu/cooker.svg" alt="OCAKLAR">
                                            <h5>OCAKLAR</h5>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-6">
                                    <div class="mega-menu-item row m-0">
                                        <a href="#">
                                            <img src="/assets/img/menu/hood.svg" alt="DAVLUMBAZLAR">
                                            <h5>DAVLUMBAZLAR</h5>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-6">
                                    <div class="mega-menu-item row m-0">
                                        <a href="#">
                                            <img src="/assets/img/menu/stove.svg" alt="FIRINLAR">
                                            <h5>FIRINLAR</h5>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-6">
                                    <div class="mega-menu-item row m-0">
                                        <a href="#">
                                            <img src="/assets/img/menu/faucet.svg" alt="ARMATÜRLER">
                                            <h5>ARMATÜRLER</h5>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-6">
                                    <div class="mega-menu-item row m-0">
                                        <a href="#">
                                            <img src="/assets/img/menu/dishwasher.svg" alt="BULAŞIK MAKİNESİ">
                                            <h5>BULAŞIK MAKİNESİ</h5>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-6">
                                    <div class="mega-menu-item row m-0">
                                        <a href="#">
                                            <img src="/assets/img/menu/eviye.svg" alt="AKSESUARLAR">
                                            <h5>AKSESUARLAR</h5>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 col-6">
                                    <div class="mega-menu-item row m-0">
                                        <a href="#">
                                            <img src="/assets/img/menu/kitchen.svg" alt="KAMPANYALAR">
                                            <h5>KAMPANYALAR</h5>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">ÜRÜNLER</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">KAMPANYALAR</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">DÖKÜMANLAR</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">YETKİLİ SERVİS NOKTALARI</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">DNA</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link languageSelector">
                            <img src="/assets/img/en.svg" alt="">
                        </a>
                    </li>
                    <li class="nav-item dropdown navbarSearch">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fal fa-search"></i>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <form class="form-inline my-2 my-lg-0">
                                <input class="form-control" type="search" placeholder="Ara" aria-label="Ara">
                                <button class="btn btn-outline-dark" type="submit">Ara</button>
                            </form>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link">
                            <i class="fal fa-comments text-success"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>