<?php

    require_once "conn.php";

    $xml = simplexml_load_file("http://akilliticaret.im/tests/xml/telefonaksesuar.xml", 'SimpleXMLElement', LIBXML_NOCDATA);
    $json = json_encode($xml);
    $array = json_decode($json,TRUE);

    foreach($array as $item) {

        foreach ($item as $nested) {

            $code = $nested['code'];
            $ws_code = $nested['ws_code'];
            $barcode = $nested['barcode'];
            $supplier_code = $nested['supplier_code'];
            $name = $nested['name'];
            $cat1name = $nested['cat1name'];
            $cat1code = $nested['cat1code'];
            $cat2name = $nested['cat2name'];
            $cat2code = $nested['cat2code'];
            $cat3name = $nested['cat3name'];
            $cat3code = $nested['cat3code'];
            $category_path = $nested['category_path'];
            $stock = $nested['stock'];
            $unit = $nested['unit'];
            $price_list = $nested['price_list'];
            $price_list_campaign = $nested['price_list_campaign'];
            $price_special_vat_included = $nested['price_special_vat_included'];
            $price_special = $nested['price_special'];
            $price_special_rate = $nested['price_special_rate'];
            $price_credit_card = $nested['price_credit_card'];
            $currency = $nested['currency'];
            $vat = $nested['vat'];
            $brand = $nested['brand'];
            $model = $nested['model'];
            $desi = $nested['desi'];
            $width = $nested['width'];
            $height = $nested['height'];
            $deep = $nested['deep'];
            $weight  = $nested['weight'];
            $detail  = $nested['detail'];
            $images1 = $nested['images']['img_item'][0];
            $images2 = $nested['images']['img_item'][1];
            $images3 = $nested['images']['img_item'][2];

            // Subproduct 1
            $subproduct_1_VaryantGroupID  = $nested['subproducts']['subproduct'][0]['VaryantGroupID'];
            $subproduct_1_code = $nested['subproducts']['subproduct'][0]['code'];
            $subproduct_1_ws_code = $nested['subproducts']['subproduct'][0]['ws_code'];
            $subproduct_1_type1 = $nested['subproducts']['subproduct'][0]['type1'];
            $subproduct_1_type2 = $nested['subproducts']['subproduct'][0]['type2'];
            $subproduct_1_barcode = $nested['subproducts']['subproduct'][0]['barcode'];
            $subproduct_1_stock = $nested['subproducts']['subproduct'][0]['stock'];
            $subproduct_1_desi = $nested['subproducts']['subproduct'][0]['desi'];
            $subproduct_1_price_list = $nested['subproducts']['subproduct'][0]['price_list'];
            $subproduct_1_price_list_discount = $nested['subproducts']['subproduct'][0]['price_list_discount'];
            $subproduct_1_price_special = $nested['subproducts']['subproduct'][0]['price_special'];
            $subproduct_1_supplier_code = $nested['subproducts']['subproduct'][0]['supplier_code'];
            $subproduct_1_images = $nested['subproducts']['subproduct'][0]['images']['img_item'][0];

            // Subproduct 2
            $subproduct_2_VaryantGroupID = $nested['subproducts']['subproduct'][1]['VaryantGroupID'];
            $subproduct_2_code = $nested['subproducts']['subproduct'][1]['code'];
            $subproduct_2_ws_code = $nested['subproducts']['subproduct'][1]['ws_code'];
            $subproduct_2_type1 = $nested['subproducts']['subproduct'][1]['type1'];
            $subproduct_2_type2 = $nested['subproducts']['subproduct'][1]['type2'];
            $subproduct_2_barcode = $nested['subproducts']['subproduct'][1]['barcode'];
            $subproduct_2_stock = $nested['subproducts']['subproduct'][1]['stock'];
            $subproduct_2_desi = $nested['subproducts']['subproduct'][1]['desi'];
            $subproduct_2_price_list = $nested['subproducts']['subproduct'][1]['price_list'];
            $subproduct_2_price_list_discount = $nested['subproducts']['subproduct'][1]['price_list_discount'];
            $subproduct_2_price_special = $nested['subproducts']['subproduct'][1]['price_special'];
            $subproduct_2_supplier_code = $nested['subproducts']['subproduct'][1]['supplier_code'];
            $subproduct_2_images = $nested['subproducts']['subproduct'][1]['images']['img_item'][0];

            // Subproduct 3
            $subproduct_3_VaryantGroupID = $nested['subproducts']['subproduct'][2]['VaryantGroupID'];
            $subproduct_3_code = $nested['subproducts']['subproduct'][2]['code'];
            $subproduct_3_ws_code = $nested['subproducts']['subproduct'][2]['ws_code'];
            $subproduct_3_type1 = $nested['subproducts']['subproduct'][2]['type1'];
            $subproduct_3_type2 = $nested['subproducts']['subproduct'][2]['type2'];
            $subproduct_3_barcode = $nested['subproducts']['subproduct'][2]['barcode'];
            $subproduct_3_stock = $nested['subproducts']['subproduct'][2]['stock'];
            $subproduct_3_desi = $nested['subproducts']['subproduct'][2]['desi'];
            $subproduct_3_price_list = $nested['subproducts']['subproduct'][2]['price_list'];
            $subproduct_3_price_list_discount = $nested['subproducts']['subproduct'][2]['price_list_discount'];
            $subproduct_3_price_special = $nested['subproducts']['subproduct'][2]['price_special'];
            $subproduct_3_supplier_code = $nested['subproducts']['subproduct'][2]['supplier_code'];
            $subproduct_3_images = $nested['subproducts']['subproduct'][2]['images']['img_item'][0];

            $query = $db->prepare("INSERT INTO products SET
                code = ?,
                ws_code = ?,
                barcode = ?,
                supplier_code = ?,
                products.name = ?,
                cat1name = ?,
                cat1code = ?,
                cat2name = ?,
                cat2code = ?,
                cat3name = ?,
                cat3code = ?,
                category_path = ?,
                stock = ?,
                unit = ?,
                price_list = ?,
                price_list_campaign = ?,
                price_special_vat_included = ?,
                price_special = ?,
                price_special_rate = ?,
                price_credit_card = ?,
                currency = ?,
                vat = ?,
                brand = ?,
                model = ?,
                desi = ?,
                width = ?,
                height = ?, 
                deep = ?,
                weight = ?,
                detail = ?,
                image_1 = ?,
                image_2 = ?,
                image_3 = ?");
            $insert = $query->execute(array(
                $code,
                $ws_code,
                $barcode,
                $supplier_code,
                $name,
                $cat1name,
                $cat1code,
                $cat2name,
                $cat2code,
                $cat3name,
                $cat3code,
                $category_path,
                $stock,
                $unit,
                $price_list,
                $price_list_campaign,
                $price_special_vat_included,
                $price_special,
                $price_special_rate,
                $price_credit_card,
                $currency,
                $vat,
                $brand,
                $model,
                $desi,
                $width,
                $height,
                $deep,
                $weight,
                $detail,
                $images1,
                $images2,
                $images3
            ));
            if ( $insert ){
                $product_id = $db->lastInsertId();

                // Sub Product 1 Insert
                $query = $db->prepare("INSERT INTO sub_products SET
                product_id = ?,
                VaryantGroupID = ?,
                code = ?,
                ws_code = ?,
                type1 = ?,
                type2 = ?,
                barcode = ?,
                stock = ?,
                desi = ?,
                price_list = ?,
                price_list_discount = ?,
                price_special = ?,
                supplier_code = ?,
                image = ?");
                $insert = $query->execute(array(
                    $product_id,
                    $subproduct_1_VaryantGroupID,
                    $subproduct_1_code,
                    $subproduct_1_ws_code,
                    $subproduct_1_type1,
                    $subproduct_1_type2,
                    $subproduct_1_barcode,
                    $subproduct_1_stock,
                    $subproduct_1_desi,
                    $subproduct_1_price_list,
                    $subproduct_1_price_list_discount,
                    $subproduct_1_price_special,
                    $subproduct_1_supplier_code,
                    $subproduct_1_images
                ));
                // Sub Product 2 Insert
                $query = $db->prepare("INSERT INTO sub_products SET
                product_id = ?,
                VaryantGroupID = ?,
                code = ?,
                ws_code = ?,
                type1 = ?,
                type2 = ?,
                barcode = ?,
                stock = ?,
                desi = ?,
                price_list = ?,
                price_list_discount = ?,
                price_special = ?,
                supplier_code = ?,
                image = ?");
                $insert = $query->execute(array(
                    $product_id,
                    $subproduct_2_VaryantGroupID,
                    $subproduct_2_code,
                    $subproduct_2_ws_code,
                    $subproduct_2_type1,
                    $subproduct_2_type2,
                    $subproduct_2_barcode,
                    $subproduct_2_stock,
                    $subproduct_2_desi,
                    $subproduct_2_price_list,
                    $subproduct_2_price_list_discount,
                    $subproduct_2_price_special,
                    $subproduct_2_supplier_code,
                    $subproduct_2_images
                ));
                // Sub Product 3 Insert
                $query = $db->prepare("INSERT INTO sub_products SET
                product_id = ?,
                VaryantGroupID = ?,
                code = ?,
                ws_code = ?,
                type1 = ?,
                type2 = ?,
                barcode = ?,
                stock = ?,
                desi = ?,
                price_list = ?,
                price_list_discount = ?,
                price_special = ?,
                supplier_code = ?,
                image = ?");
                $insert = $query->execute(array(
                    $product_id,
                    $subproduct_3_VaryantGroupID,
                    $subproduct_3_code,
                    $subproduct_3_ws_code,
                    $subproduct_3_type1,
                    $subproduct_3_type2,
                    $subproduct_3_barcode,
                    $subproduct_3_stock,
                    $subproduct_3_desi,
                    $subproduct_3_price_list,
                    $subproduct_3_price_list_discount,
                    $subproduct_3_price_special,
                    $subproduct_3_supplier_code,
                    $subproduct_3_images
                ));

            }

        }

    }