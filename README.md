<h3>Demo E-Ticaret</h5>

<br />

<h5>Geliştirmeler:</h5>
<ul>
	<li>İlk etapta frontend geliştirmesi ve e-posta bülteni aboneliği gerçekleştirildi.</li>
	<li>XML Dosyası ile canlı link üzerinden ürünler veritabanına aktarıldı. <b>( /includes/get-xml.php )</b></li>
	<li>Veritabanına kaydedilen ürünler websitesi üzerinde listelendi.</li>
</ul>

<h5>Kullanım:</h5>
<ul>
	<li>1. Dosyaların clonunu alınız.</li>
	<li>2. Mail ile gönderilen veritabanını import ediniz ve veritabanı bilgilerini /includes/conn.php dosyasından değiştiriniz.</li>
	<li>3. Headerda bulunan ÜRÜNLER linkine tıklayarak tüm ürünlere ulaşabilirsiniz. ( <b>/category.php</b> )</li>
	<li>4. Herhangi bir ürünün üzerinde tıklayarak detaylarına erişebilirsiniz. ( <b>/product.php?id=?</b> )</li>
</ul>